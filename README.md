# MDSI Student Guide

This is a living document collaboratively authored by students in the Master of Data Science and Innovation course at the University of Technology Sydney. For information about the course, please visit: http://www.uts.edu.au/future-students/analytics-and-data-science

To view the book, go to: https://utsmdsi.bitbucket.io/

## Contributions

To edit or create content in this book:

1. Fork the repository into your own account
1. Make and commit changes in your forked repository
1. Raise a pull request to merge your changes back into the master branch in this repository

## Build

When a change is merged onto the master branch, the book will be recompiled via Bitbucket Pipelines and published to the website automatically. 
