# Contributors to FlipAround

```{r, results = "asis", echo = FALSE, message = FALSE}
library(dplyr)
contributors <- 
  readr::read_csv("thanks.txt", col_names = 'name') %>%
  arrange(name)
cat("**Special thanks** to the UTS staff and students who assisted with reviewing 
and editing this student manual: ")
cat(paste(contributors$name, collapse = ", "))
cat('.')
```

## If you want to contribute

Go to the [Bitbucket repo](https://bitbucket.org/utsmdsi/utsmdsi.bitbucket.io) for more information.

